from datetime import datetime
import unittest
from unittest.mock import Mock
from app import create_app

from app.Infrastructure.daily_price_crawler import WebPageCrawl
from app.database import db, populate
from app.Domain.prices import PriceData as PriceRep


class SeleniumCrawl(unittest.TestCase):

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        populate()
        day = 2
        month = 4
        year = 1402
        self.crawler = WebPageCrawl(year, month, day)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_web_crawl_1(self):
        self.crawler.do()


if __name__ == "__main__":
    unittest.main()
