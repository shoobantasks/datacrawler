import os
import click

from flask.cli import FlaskGroup
from app import create_app
from app.database import db, populate
from flask_migrate import Migrate


app = create_app(os.getenv('APP_ENV') or 'production')

migrate = Migrate(app, db, command='database')
migrate.init_app(app)

cli = FlaskGroup(app)


@cli.command
def populate_db():
    """Populates database tables"""
    populate()


if __name__ == '__main__':
    cli()
