import os
from flask import Flask

from config import config as Config
from app.database import db
from app.crawl import crawl as crawl_blueprint


def create_app(config_name):
    app = Flask(__name__)

    if not isinstance(config_name, str):
        config_name = os.getenv('APP_ENV', 'production')

    app.config.from_object(Config[config_name])
    Config[config_name].init_app(app)

    init_extensions(app)

    register_blueprints(app)

    return app


def init_extensions(app):
    db.init_app(app)

def register_blueprints(app):
    app.register_blueprint(crawl_blueprint)
