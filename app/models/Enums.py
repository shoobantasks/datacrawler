from enum import Enum


class AnimalType(Enum):
    SHEEP = 'Sheep'
    CALF = 'Calf'
    