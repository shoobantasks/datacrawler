from datetime import datetime
from sqlalchemy import Enum, Column, Integer, String, Time, Date, UniqueConstraint, ForeignKey
from sqlalchemy.orm import relationship

from app.database import db
from app.models.Enums import AnimalType


class PriceDataModel(db.Model):
    """price_data Database Table"""
    __tablename__ = "price_data"
    __table_args__ = (UniqueConstraint('animal_type', 'shamsi_date', 'state_id', name='_type_date_state_uc'),)

    id = Column(Integer, primary_key=True)
    animal_type = Column(Enum(AnimalType))
    min_price = Column('min_price', Integer, nullable=False)
    max_price = Column('max_price', Integer, nullable=False)
    avg_price = Column('avg_price', Integer, nullable=False)
    time = Column('time', Time, nullable=False)
    date = Column('date', Date, default=datetime.now().date(), nullable=False)
    shamsi_date = Column('shamsi_date', Integer, nullable=False)
    state_id = Column(Integer, ForeignKey('state.id'))

    state = relationship('State')

    @property
    def serialize(self):
        return {
            'id': self.id,
            'animal_type': self.animal_type,
            'min_price': self.min_price,
            'max_price': self.max_price,
            'avg_price': self.avg_price,
            'time': self.time,
            'date': self.date,
            'shamsi_date': self.shamsi_date,
        }

class State(db.Model):
    """state Database Table"""
    __tablename__ = "state"

    id = Column(Integer, primary_key=True)
    title = Column('Title', String(25), nullable=False)
