from datetime import date, time
from typing import Dict, List, Optional
# from sqlalchemy import insert
from sqlalchemy.dialects.postgresql import insert
import jdatetime

from app.database import db
from app.models.prices import PriceDataModel


class PriceData:
    ### Price db functions ###
    def allPrices(self):
        return db.session.query(PriceDataModel).all()

    def newPrice(self, min_price: int, max_price: int, avg_price: int, time: time) -> None:
        PriceToAdd = PriceDataModel(
            min_price=min_price,
            max_price=max_price,
            avg_price=avg_price,
            time=time,
            shamsi_date = int(str(jdatetime.date.today()).replace('-', ''))
        )
        db.session.add(PriceToAdd)
        db.session.commit()

    def bulkisertPrice(self, values: List[PriceDataModel]) -> None:
        # statement = insert(PriceDataModel)\
        #     .values([v.serialize for v in values])\
        #     .on_conflict_do_nothing()\
        #     .returning(PriceDataModel.id)
        #     # .on_conflict_do_nothing(index_elements=['id'])\
        # price_ids = [price_id for price_id, in db.session.execute(statement)]
        # return price_ids
        db.session.add_all(values)
        db.session.commit()

    def editPrice(self, Price_id: int, min_price: Optional[int], max_price: Optional[int], 
                avg_price: Optional[int], time: Optional[time], date: Optional[date], shamsi_date: Optional[int]) -> None:
        PriceToEdit = self.getPrice(Price_id)
        if not min_price:
            PriceToEdit.min_price = min_price
        if not max_price:
            PriceToEdit.max_price = max_price
        if not avg_price:
            PriceToEdit.avg_price = avg_price
        if not time:
            PriceToEdit.time = time
        if not date:
            PriceToEdit.date = date
        if not shamsi_date:
            PriceToEdit.shamsi_date = shamsi_date
        db.session.add(PriceToEdit)
        db.session.commit()

    def deletePrice(self, Price_id: int) -> None:
        PriceToDelete = self.getPrice(Price_id)
        db.session.delete(PriceToDelete)
        db.session.commit()

    def getPrice(self, Price_id: int) -> PriceDataModel:
        return db.session.query(PriceDataModel).filter_by(id=Price_id).one()
