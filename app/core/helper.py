def persian_to_english_numbers(text: str) -> str:
    map_numbers ={'۰':'0', '۱':'1', '۲':'2', '۳':'3', '۴':'4', '۵':'5', '۶':'6', '۷':'7', '۸':'8', '۹':'9'}
    return ''.join([map_numbers[c] if c in map_numbers else c for c in text])
