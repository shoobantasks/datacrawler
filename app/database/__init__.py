from flask_sqlalchemy import SQLAlchemy

from app.models.prices import State

db = SQLAlchemy()

def populate():
    states = [
        State(title='تهران'),
        State(title='اصفهان'),
        State(title='خراسان رضوی'),
        State(title='فارس'),
        State(title='همدان'),
        State(title='قم'),
        State(title='مرکزی'),
        State(title='چهارمحال بختياري')
    ]
    db.session.add_all(states)
    db.session.commit()
