import os
import re
import time
import pandas as pd
from abc import ABC, abstractmethod
from typing import Dict, List
from urllib import request

from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.by import By

from selectorlib import Extractor
import jdatetime

from app.Domain.prices import PriceData as PriceRep
from app.core.helper import persian_to_english_numbers
from app.models.Enums import AnimalType
from app.models.prices import PriceDataModel


class DailyCrawler(ABC):
    url: str
    day: int
    month: int
    year: int

    def __init__(self, url: str, year: int, month: int, day: int) -> None:
        if not self.shamsi_date_validate(year, month, day):
            raise ValueError('Your selected date is invalid!')
        self.url = url
        self.day = day
        self.month = month
        self.year = year
        super().__init__()

    def crawl(self) -> str:
        """get data from url"""
        r = request.get(self.url)
        return r.text

    @abstractmethod
    def parse(self, content: str):
        """parse html data, should be implement in subclasses"""
        raise NotImplementedError("Subclasses should implement this!")
    
    def do(self):
        content = self.crawl()
        data = self.parse(content)
        self.__save_to_db__(data)
        self.__save_to_excel__(data)

    def __save_to_db__(self, data: List[PriceDataModel]) -> None:
        """ save response data to postgresql"""
        PriceRep().bulkisertPrice(data)

    def __save_to_excel__(self, data: List[PriceDataModel]) -> None:
        """ save response data to excel, name of file is shamsi date"""
        shamsi_date = data[0].shamsi_date
        df = pd.DataFrame([d.serialize for d in data])
        if not os.path.exists('files'):
            os.mkdir('files')
        df.to_excel(f'files/{shamsi_date}.xlsx')

    def shamsi_date_validate(self, year: int, month: int, day: int) -> bool:
        today = jdatetime.date.today()
        current_year = today.year
        current_month = today.month
        current_day = today.day

        return (year > 1300 and 0 < month and 0 < day and 
                ((year < current_year) or (year == current_year and month < current_month) 
                 or (year == current_year and month == current_month and day < current_day)))


class RssCrawl(DailyCrawler):
    def __init__(self, year: int, month: int, day: int) -> None:
        url: str = f'http://www.itpnews.com/Apps/get/price.php?cal_years={year}&cal_months={month}&cal_days={day}&id=301&mod=#price'
        super().__init__(url, year, month, day)


class WebPageCrawl(DailyCrawler):
    def __init__(self, year: int, month: int, day: int) -> None:
        url: str = 'https://www.itpnews.com/price/show/%D8%AF%D8%A7%D9%85+%D8%B2%D9%86%D8%AF%D9%87/301'
        super().__init__(url, year, month, day)

    def crawl(self) -> str:
        content = ''
        try:
            driver = webdriver.Firefox()
            driver.get(self.url)
        
            day_element = driver.find_element(By.NAME, "price_d")
            form = day_element.find_element(By.XPATH, './../..')
            for element in form.find_elements(By.TAG_NAME, 'select'):
                web_el = Select(element)
                web_el.select_by_value(str(self.year) if 'price_y' in element.get_attribute('name') 
                                        else str(self.month) if 'price_m' in element.get_attribute('name') 
                                        else str(self.day))
            form.submit()
        
            time.sleep(5)
        
            title_element = driver.find_element(By.CSS_SELECTOR, '.titleTop h2')
            title = title_element.get_attribute('outerHTML')
            table_element = driver.find_element(By.CLASS_NAME, 'price_table')
            result_table = table_element.get_attribute('outerHTML')
            content = title + result_table
        except Exception as ex:
            # notify to admin : "change html pattern in source page"
            raise ex
        finally:
            driver.close()
        return content
    
    def parse(self, content: str):
        date_pattern = "[0-9]{2}\\/[0-9]{2}\\/[0-9]{2}"
        result_extractor = Extractor.from_yaml_file('app/extractors/price1.yml')
        data = []
        result = result_extractor.extract(content)
        title = persian_to_english_numbers(result['title'])
        date_parts = [int(part) for part in f"14{re.findall(date_pattern, title)[0]}".split('/')]
        shamsi_date = jdatetime.date(date_parts[0], date_parts[1], date_parts[2], locale='fa_IR')
        for item in result['items'][2:-1]:
            data.append(PriceDataModel(
                animal_type=AnimalType.SHEEP,
                min_price=item['min_sheep'].replace(',', ''),
                max_price=item['max_sheep'].replace(',', ''),
                avg_price=int(item['min_sheep'].replace(',', '')) + int(item['max_sheep'].replace(',', '')) / 2,
                time=item['time'],
                date=shamsi_date.togregorian(),
                shamsi_date=int(shamsi_date.strftime("%Y%m%d"))
            ))
            data.append(PriceDataModel(
                animal_type=AnimalType.CALF,
                min_price=item['min_calf'].replace(',', ''),
                max_price=item['max_calf'].replace(',', ''),
                avg_price=int(item['min_calf'].replace(',', '')) + int(item['max_calf'].replace(',', '')) / 2,
                time=item['time'],
                date=shamsi_date.togregorian(),
                shamsi_date=int(shamsi_date.strftime("%Y%m%d"))
            ))

        return data
